# VR Designer

## Summary
The app would be a photoshop/adobe XD like app but will be accessed through a VR headset and Kinect. It allows users to edit pictures and design apps using their hands to move, resize, and edit different aspects of an image or app workflow. To use the app the user would need a Microsoft Kinect ($99) and either google cardboard ($15-$30) or Samsung Gear VR ($50). The Kinect would track the users body movements and allow them to control their design with their hands. The app could be used in school art classes as a new medium for creating artwork. It would provide a more active and immersive experience for students. Also with using more cost friendly solutions to create the app it wouldn’t be out of the questions to get multiple stations. It is also much for cost friendly for at home user, as many kids theses days have a Kinect already if they or their parents have a Xbox product.  Potentially we could make it so their could be multiple users using the same Kinect to work on the same photo or app flow.

## Requirements
- Microsoft Kinect or Xbox Kinect (with USB adapter)
- Mobile VR Headset (Google Cardboard or Samsung Gear VR)
- Windows PC

## Features
### Basic Features
- Choose different colors
- Import pictures
- 2D shapes (circle,ellipse,square,rectangle)


### Intermediate (If we have time)
- Draw a line through space.
- Crop pictures
- Resize pictures
- 3D Shapes (sphere, cube, cylinder)

### Advanced (Stretch)
  - Collaborate with others on art
	
